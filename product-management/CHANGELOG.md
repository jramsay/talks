# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [1.0.0] - 2019-09-02

- Re:coded House (Erbil, Iraq)

[1.0.0]: https://gitlab.com/jramsay/talks/commit/eb14f7f7c49fac115402b3cb8b3ae92f6b2cd297
