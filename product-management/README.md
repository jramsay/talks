# Product Management 101

It is easier than ever to build and launch digital products, but knowing what to build is hard.
How do we make sure what we design and develop will be useful?
Will our customers love what we build? And what should we build next?

This workshop will teach you about the role of the Product Manager in modern software development teams,
the principles of product management, and how to apply them in your own projects.

[![Preview of slides](preview.png)](slides.pdf)

## Resources

- [GitLab Handbook](https://about.gitlab.com/handbook/product)
- [Intercom](https://www.intercom.com/books/product-management)
- [Shape Up (Ryan Singer, Basecamp)](https://basecamp.com/shapeup)
- [When Coffee and Kale Compete](https://www.whencoffeeandkalecompete.com/)

## Presented

- 2019-09-02: Re:coded House (Erbil, Iraq)
