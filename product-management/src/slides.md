slidenumbers: false
autoscale: true

# <br>
# [Fit] Product
# [Fit] Management
# <br>
#### *James Ramsay – September 2019*

^ Who is here? Developers? Product Manager? Entrepreneur/Small Business?


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Sans]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.quote: #FFFFFF, Vulf Sans]

> <br>
> <br>
> “Discover a product
> that is valuable, usable, and feasible”
-- *Marty Cagan, Inspired*

^ Create something new, useful and valuable.

^ Discovery, learning. Handling uncertainty.

^ What should we build next? And why?

^ Creating value.

^ Why aren't my customers happy?

^ Important and hard question


---
[.background-color: #FFFFFF]

![nudge](images/nudge.png)

^ How I became a product manager


---
## [Fit] I did it _wrong_


---
![Why aren't you using it](images/interview1.png)


---
![I don't have time](images/interview2.png)


---
## Ask *why*,
## not what.

^ Easies mistake to make.

^ Very hard to avoid.

^ Focussed on getting the pharmacist to use the software.

^ Metric of success 


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Sans, alignment(left)]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]

## James Ramsay
### [fit] *Senior Product Manager - GitLab*

![left](images/gitlab-summit.jpg)


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Sans]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

### <br>
### What advice would
### I give myself?

^ If I could go back in time, what would I tell myself?


---
### [fit] *4* tools, and
### [fit] *3* principles


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Principle № 1 —**
# [fit] Direction

^ Time check: 10mins

^ Begin with a direction

^ Think about the direction

^ Always consider the direction


---
### [fit] *Imagine where you want to be*

^ What will be possible tomorrow, that isn't possible today

^ Maybe the idea that started the company

^ Not a list of features

^ Features and products change over time


---
![direction](images/direction.png)

^ Make it real. Write it down. Draw pictures.

^ Communicate it with everyone: up and down

^ Don't let it be words on the wall, or the about page of a website

^ When we have a list of features, we call it a roadmap?

^ Where is it a map to?


---
> <br>
> <br>
> “Replace disparate DevOps toolchains with a single application that works by default across the entire DevOps lifecycle”
-- *GitLab vision*

^ Solving a problem

^ Does not prescribe a solution

^ Does prescribe an approach

^ Strong hypotheses


---
### [fit] *Bring the future closer*

^ Direction makes the future tangible

^ Now I can try to bring it closer

^ Know your customers and their challenges well


---
### [fit] Keep asking “*Why?*”

^ PM should spend 70% of our time understanding the problem

^ Should be talking to at least 5 customers

^ Ask why, not what!

^ Avoid imagining concrete solutions.

^ Ask lots of context questions, obvious stuff

^ https://www.nngroup.com/articles/why-you-only-need-to-test-with-5-users/


---

### [fit] You are *not*
### [fit] the customer

^ Often begin as our customer.

^ Started company because we wished it existed.

^ Built something for ourselves.

^ As soon as it's a product, there are many customers.

^ You might have ideas, they might not be the best ideas. Opinions/ideas are valid. Be open to the fact that a customer might have a better idea. - Sidebar that's why GitLab is open source.

---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Example —**
# [fit] Web IDE
### [fit] _(Integrated Development Environment)_

---
![problem](images/webide-problem.png)

^ Rails girls story

^ Graveyard of failed startups


---
### [fit] *Understand the problem(s)*

^ Editing, saving, previewing files is hard

^ Eventually solve the big scary problem.

^ Incredibly difficult to focus on customer and not ourselves.

---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Tool —**
# [fit] JTBD
### _Jobs To Be Done_

^ Framework that helps you understand why and how people buy the products they do.


---
### [fit] *Hiring a product for progress*

^ People buy things to help them make progress - it isn't random

^ The are seeking an outcome or a better state

^ Causality: what caused them to switch yesterday, or the day before?

^ Tool: switch interview


---
[.footer: https://www.useronboard.com/features-vs-benefits/]

![benefits](images/benefits.png)

^ People don't buy products or features

^ People do buy progress


---
[.footer: https://www.intercom.com/blog/mattresses-using-jobs-done-research-software/]
![fit](images/jtbd-forces.png)

^ Differences between what customers say and do

^ Important to understand the context

^ Begins with first thought


---

### [fit] *Focus on causality*

^ What was the situation?

^ What were they feeling?

^ What happened the weekend before?

^ Bring it back to the Web IDE


---
### [fit] *When _____________ __← ... Situation__* 
### [fit] *I want to ________ __← .. Motivation__*
### [fit] *So I can _________ __← .... Progress__*

^ Include context in the situation

^ Source should be real people, not personas

^ Jobs should be independent of a solution

^ Consider the forces of progress

^ Jobs can be from multiple points of view


---
[.text: #000000, Vulf Sans, alignment(left)]
[.text-emphasis: #DB3B21, Vulf Sans Italic]

*When* I receive feedback on a merge request that is nearly complete, and I have already begun working on something else, *I want to* resolve the feedback without completely breaking my flow by reconfiguring my local development environment and possibly losing work, *so I can* feel more productive and spend less time fiddling with my tooling and more time writing code.


---
[.text: #000000, Vulf Sans, alignment(left)]
[.text-emphasis: #DB3B21, Vulf Sans Italic]

*When* I notice inconsistencies or areas needing improvement in the GitLab Handbook and I decide I want to fix it myself, *I want to* make my changes right away before I need to join my next meeting or interrupted by something else, *so I can* ...


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Principle № 2 —**
# [fit] Iteration

^ Time check: 10mins

^ Cagan describes product as form of discovery

^ Cannot be certain of outcome

^ Path is not known


---
### [fit] *Learn from every iteration*

^ Trial and error, or a scientific approach

^ Form an explicit hypothesis

^ How will we measure success, can we invalidate this hypothesis?

^ Question: do you believe that is the truth or is that a hypothesis


---
### [fit] *Reduce risk of uncertainty*

^ If Bookface is a better version of Facebook, people will use Bookface

^ Reduce time between iterations

^ Waste two weeks rather than three months


---
![iteration](images/iteration-why.png)

^ Increase velocity

^ Emphasize learning

^ Desired outcome faster

^ Cycle time compression may be the most underestimated force in determining winners and losers in tech. — Marc Andreessen


---
[.footer: https://blog.crisp.se/2016/01/25/henrikkniberg/making-sense-of-mvp]

![iteration](images/iteration-how.png)

^ Incremental delivery is not iteration

^ Testable hypotheses are critical


---
### Low level of *shame*

^ Polished marketing and perfect execution on your first iteration might be an indication you haven't tested your hypothesis early enough.


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Tool —**
# [fit] MVC
### [fit] _Minimum Viable Change_


---
### [fit] Smallest experiment
### [fit] to test a hypothesis

^ https://svpg.com/minimum-viable-product/
^ http://www.startuplessonslearned.com/2009/08/minimum-viable-product-guide.html


---
### Be concrete. Be visual.

^ Should be in the direction of where you need to go.

^ Should be really visual, concrete.


---
### [fit] *Problem to be solved __← Job Story/Hypothesis__* 
### [fit] *Further details .... __← Situational Analysis__*
### [fit] *Proposal ........... __← ......... Actionable__*
### [fit] *Measure success .... __← ....... Quantitative__*

^ Write it down in a document.

^ Explain the causality between the situation and job to be done.


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Example —**
# [fit] Code Owners
### [fit] _Approvals based on files changed_


---
![problem](images/codeowners-problem.png)

^ Goldman Sachs along with a few other customers came to us

^ GitLab also had this problem

^ Require approval.

^ Simpler problem: before I start making a change who do I talk to?


---
![fit](images/codeowners-show.png)

^ Who is responsible for this file?

^ 17 people participating in the issues


---
![fit](images/codeowners-detail.png)

^ Go to the file to find out

^ People found this useful

^ People gave us feedback

---
![fit](images/codeowners-suggest.png)

^ Approvals and code owners seemed like a natural fit

^ More people told us they wanted 

---
![fit](images/codeowners-assign.png)

^ 100 hundred participants

^ 69 thumbs ups

---
![fit](images/codeowners-require.png)


---
![fit](images/codeowners-branch.png)


---
![fit](images/codeowners-multiple.png)


---
### Code owners: iterations

- Show
- Suggest approvers
- Assign approvers
- Require approval
- Require approval per branch
- Multiple code owner files


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Principle № 3 —**
# [fit] Prioritization

^ Time check: 30mins

---
### [fit] *What should we do next?*

^ Why!?

^ You've got ideas

^ Customer requests

^ Sales requests

^ Systematic and logical approach


---
### Limited resources

^ Time, money

^ Dilution

^ Careful of sunk cost fallacy.


---
### Uncertainty

^ A more uncertain item might have a bigger reward.

^ Be deliberate and be focussed.


---
### Company strategy

^ Take a high level view

^ Short term growth vs long term revenue

^ How does your company measure success? Revenue, growth, breadth?


---
### No perfect answer

^ Where does this leave us?


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]
#### **— Tool —**
# [fit] RICE
### [fit] _Reach Impact Confidence Effort_

^ Method of scoring big features or projects


---
### Avoid natural biases

^ Pet projects you'd use yourself vs broad reach

^ Clever ideas vs direction

^ New ideas vs high confidence well understood

^ Discount relative effort

---
## Reach
### [fit] *people/events per time period*

^ Avoid bias to yourself

^ Customers per quarter

^ Transactions per month

^ Use real numbers


---
## Impact
### [fit] *Minimal ___ Low ___ Medium ___ High ___ Massive*
### [fit] *0.25 ______ 0.5 _____ 1 _______ 2 ___________ 3*

^ Aligned to a business goal

^ Increase adoption

^ Imprecise rating

^ This scales our other ratings


---
## Confidence
### [fit] *Moonshot ____________ Low ___ Medium ___ High*
### [fit] *0 ___________________ 50 ______ 80 ______ 100*

^ Check our enthusiasm for exciting but ill-defined ideas

^ Percentage, best to use multiple choice scale to keep it simple


---
## Effort
### [fit] *person months of product, design & engineering*

^ Maximize impact with available people

^ No less than 1, unless absolutely tiny


---
$$
RICE\;Score = \frac{reach \times impact \times confidence}{effort}
$$

^ I recommend creating a spreadsheet for scoring and sorting


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Tool —**
# [fit] Product Discovery
### [fit] _Prototypes, Mockups, Investigation_

^ Time check: 40mins

^ Cheaper than building it!


---
### [fit] *Schedule time for investigation*

^ Interviewing customers

^ Working with UX on mockups

^ Investigating technical implementation


---
### Have a clear objective


---
### Be open to alternatives


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]

## [fit] How to succeed as a
## [fit] _Product Manager_


---
### [fit] 1. Navigating the organization

^ Engineers

^ Executives and leaders

^ Sales

^ Support

^ Infrastructure

^ People Operations


---
### [fit] 2. Focussing on results

^ Company, team and product should be aligned


---
### [fit] 3. Leading with the vision

^ Connect the present work with where the direction

^ Help people understand why


---
### [fit] 4. Communication skills

^ Write everything down

^ Share it with others

^ Be actionable: clear with what you are requesting


---
### [fit] 5. Always be humble

^ Product managers don't need to come up with all the ideas

^ Ideas don't even matter that much, it's how you execute

^ Customers, engineers etc have great ideas

^ Seek out alternative opinions


---


---
## <br>
## :coffee: Coffee Break
## <br>
### [fit] *🦊 GitLab is hiring! → about.gitlab.com/jobs*

---


---
[.background-color: #000000]
[.header: #FFFFFF, Vulf Mono]
[.header-emphasis: #FFFFFF, Vulf Mono Italic]
[.header-strong: #FFFFFF, Vulf Mono Bold]

#### **— Exercise —**
# [fit] Think Big
# [fit] Think Small


---
### <br>
### [fit] Choose a Topic


---
### <br>
### Think
### [fit] Big
#### *20 minutes*


---
# <br>
#  Think
### Small
#### *20 minutes*


---


---
# [fit] Questions?


---
# Resources

GitLab Handbook
https://about.gitlab.com/handbook/product

Intercom
https://www.intercom.com/books/product-management

Shape Up (Ryan Singer, Basecamp)
https://basecamp.com/shapeup

When Coffee and Kale Compete
https://www.whencoffeeandkalecompete.com/


---
#### <br>
#### jramsay@gitlab.com
#### www.jramsay.com.au
#### <br>
#### [fit] *🦊 GitLab is hiring! → about.gitlab.com/jobs*
